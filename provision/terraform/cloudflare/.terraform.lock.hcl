# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/carlpett/sops" {
  version     = "0.7.0"
  constraints = "0.7.0"
  hashes = [
    "h1:+3Fmy7UyAwy9vmw7Rl2BvCoVGbpCmrOuvnM/0cBwODA=",
    "zh:07eca9b5fc217f3a2724df799a9cea3c15ae9b8dc49ce9bf05c88256ebd0b5dd",
    "zh:25dfde76ec264409dd3cfc8b8720d78d45892e236bf12a7e13602b1ecf438b76",
    "zh:2e81bb741a16ced03e747c8df94572fa7de387e99adf8b3033ecd857d6abf44b",
    "zh:8c1c7626b35193f425b9525b88ef9d76d9c03828aa185bd4a9ffa633b0261c90",
    "zh:a7c33a77638a39b3c90865ac0d9584977a878c8154bd96069c6e79b41ab9f711",
    "zh:d72ae184b4898f418bce1a635133ce6e716174076e65724752a8d187699de07a",
    "zh:fe9f72da6580e4446289e3408b12e21a4ebbd3f8652abc71d50f39ca3934058d",
  ]
}

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.12.1"
  constraints = "3.12.1"
  hashes = [
    "h1:9F6V3VfOK3ZiGzzZ1neeHJq0NrEBxSKNp0LUXZHB4Dc=",
    "zh:31e3d5e0f213c1042ec16284787e9577a7ece14026e5cdab111987db7e9a6318",
    "zh:33f9c4b0d70d3e0b2668b0f57ee1397bd6ccfa540e849da2de2d6143bbaffca5",
    "zh:452dbc38a2415f111e91bf42386cec744b7c5b4d7d7f063cde8b3d53f0c585ad",
    "zh:586c580350f633e04799514dfe73b84d5f7d497eda84b9ce51e2c6630ca15d46",
    "zh:786bc5e47330cdd1a14e03c728df9884967929bc5da524250d7c9198cec8661f",
    "zh:94757c9a72c8a93d9b069ebf1837bc4d2a1f53fd4a7fc45f70c722fd7e8d5954",
    "zh:99e170e7d1b4941bf801a5797d88fb81b20fd8dedb63f0956b95c44988d25830",
    "zh:bf9696a7d534fc755ab519bab969fe177d762e020af14ccee6fd0b343507b9c5",
    "zh:c203122dfdf37b49cbed2466ee82ab8a56e95a33e92d8d386b5d29347286b1f8",
    "zh:c3098cce493a9cf9a40863bcd867d84ce13b33114b19576d7e0b5fd191ffadfc",
    "zh:d822ff9e8607e03af7e248e1b48c790fa7d4b17094dadbf1b55dfdb441693908",
    "zh:d9cc8049679fdcd18ffb24cce2a19f7f1b988bd24c80dbe3b02e026dfbf797c5",
    "zh:f2a21e1a79ce8e4fcd11529488e8368f99f06a1c8727f94e071eecfb08994f6f",
    "zh:f9b7a781a04d79086333731a1a1986cf6871e02ccbb7f5fe0e8fcd487bb250d1",
  ]
}

provider "registry.terraform.io/hashicorp/http" {
  version     = "2.1.0"
  constraints = "2.1.0"
  hashes = [
    "h1:HmUcHqc59VeHReHD2SEhnLVQPUKHKTipJ8Jxq67GiDU=",
    "zh:03d82dc0887d755b8406697b1d27506bc9f86f93b3e9b4d26e0679d96b802826",
    "zh:0704d02926393ddc0cfad0b87c3d51eafeeae5f9e27cc71e193c141079244a22",
    "zh:095ea350ea94973e043dad2394f10bca4a4bf41be775ba59d19961d39141d150",
    "zh:0b71ac44e87d6964ace82979fc3cbb09eb876ed8f954449481bcaa969ba29cb7",
    "zh:0e255a170db598bd1142c396cefc59712ad6d4e1b0e08a840356a371e7b73bc4",
    "zh:67c8091cfad226218c472c04881edf236db8f2dc149dc5ada878a1cd3c1de171",
    "zh:75df05e25d14b5101d4bc6624ac4a01bb17af0263c9e8a740e739f8938b86ee3",
    "zh:b4e36b2c4f33fdc44bf55fa1c9bb6864b5b77822f444bd56f0be7e9476674d0e",
    "zh:b9b36b01d2ec4771838743517bc5f24ea27976634987c6d5529ac4223e44365d",
    "zh:ca264a916e42e221fddb98d640148b12e42116046454b39ede99a77fc52f59f4",
    "zh:fe373b2fb2cc94777a91ecd7ac5372e699748c455f44f6ea27e494de9e5e6f92",
  ]
}
